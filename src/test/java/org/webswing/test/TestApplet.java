package org.webswing.test;

import netscape.javascript.JSObject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Vector;

public class TestApplet extends JApplet {

	@Override
	public void start() {
		//		try {
		//			Class<?> tinyCls = Class.forName("net.sf.tinylaf.TinyLookAndFeel");
		//			UIManager.setLookAndFeel((LookAndFeel) tinyCls.newInstance());
		//		} catch (UnsupportedLookAndFeelException e) {
		//			e.printStackTrace();
		//		} catch (Exception e) {
		//			System.out.println("TinyLaf not on classpath =" + e.getMessage());
		//		}
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		JPanel infopanel = new JPanel(new GridLayout(2, 3));
		JButton button = new JButton("Applet:" + getParameter("type"));
		button.addActionListener(new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
//				PrinterJob job = PrinterJob.getPrinterJob();
//				PageFormat pf = new PageFormat();
//				job.setPrintable(new Printable() {
//					@Override
//					public int print(Graphics g, PageFormat pf, int page) throws PrinterException {
//						if (page > 0) { /* We have only one page, and 'page' is zero-based */
//							return NO_SUCH_PAGE;
//						}
//						Graphics2D g2d = (Graphics2D) g;
//						panel.printAll(g);
//						return PAGE_EXISTS;
//					}
//				}, pf);
//				boolean ok = job.printDialog();
//				if (ok) {
//					try {
//						job.print();
//					} catch (PrinterException ex) {
//					}
//				}
				JOptionPane.showMessageDialog(null,"TESTMODAL");
				JSObject.getWindow(null).eval("open('frame_d.html','_blank')");
			}
		});
		infopanel.add(button);
		infopanel.add(new JTextField());
		JMenu menu = new JMenu("menu");
		menu.add(new JMenuItem("1"));
		menu.add(new JMenuItem("2"));
		menu.add(new JMenuItem("3"));
		menu.add(new JMenuItem("4"));
		menu.add(new JMenuItem("5"));
		menu.add(new JMenuItem("6"));
		menu.add(new JMenuItem("7"));
		menu.add(new JMenuItem("8"));
		menu.add(new JMenuItem("9"));
		JButton logout = new JButton("logout");
		logout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JSObject.getWindow(null).eval("webswingAppletWrapper.closeAllAndReload()");
			}
		});
		infopanel.add(logout);
		infopanel.add(new JColorChooser());
		infopanel.add(new JComboBox<String>(new Vector<>(Arrays.asList("1", "2", "3", "4", "5", "6"))));
		infopanel.setBorder(BorderFactory.createTitledBorder("Applet Information"));
		panel.add(BorderLayout.CENTER, infopanel);
		add(panel);
	}
}
