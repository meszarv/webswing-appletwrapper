package org.webswing.appletwrapper.javascript;

import org.webswing.appletwrapper.AppletManager;
import org.webswing.appletwrapper.config.ConfigUtil;
import org.webswing.appletwrapper.model.AppletSync;
import org.webswing.appletwrapper.model.AppletSyncError;
import org.webswing.appletwrapper.model.SyncRequest;
import org.webswing.appletwrapper.model.SyncResponse;
import org.webswing.toolkit.util.Logger;

import javax.swing.JDialog;
import java.awt.KeyboardFocusManager;
import java.awt.Window;
import java.io.IOException;
import java.util.Map;

public class JsService {
	private final AppletManager mgr;

	public JsService(AppletManager mgr) {
		this.mgr = mgr;
	}

	public String sync(String json) {
		SyncResponse response = new SyncResponse();
		try {
			Map<String, Object> syncData = JsUtil.mapper.readValue(json, Map.class);
			SyncRequest request = ConfigUtil.instantiateConfig(syncData, SyncRequest.class);
			for (AppletSync item : request.getSyncItems()) {
				AppletSyncError result = null;
				switch (item.getAction()) {
				case create:
					result = mgr.createApplet(item.getName(), item.getId(), item.getPosition(), item.getParameters());
					break;
				case delete:
					result = mgr.deleteApplet(item.getId());
					break;
				case update:
					result = mgr.updateApplet(item.getId(), item.getPosition(),item.isVisible());
					break;
				}
				if (result != null) {
					response.getResults().add(result);
				}
			}
			response.setWindowAreas(JsUtil.getWindowAreas());
			response.setModalFlag(JsUtil.isModalFocused());
			return JsUtil.mapper.writeValueAsString(response);
		} catch (IOException e) {
			Logger.error("Failed to process sync message", e);
			return null;
		}
	}

	public String getInitialUrl() {
		return mgr.getInitialUrl();
	}
}
