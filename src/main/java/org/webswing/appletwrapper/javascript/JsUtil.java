package org.webswing.appletwrapper.javascript;

import com.fasterxml.jackson.databind.ObjectMapper;
import netscape.javascript.JSObject;
import org.webswing.appletwrapper.Main;

import javax.swing.JDialog;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.Window;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class JsUtil {

	public static final ObjectMapper mapper = new ObjectMapper();

	public static void installJavascript(JsService jsService) {
		JSObject window = JSObject.getWindow(null);
		System.setProperty("webswing.appletDocumentBase", (String) window.eval("location.href"));
		String base_path = System.getProperty("webswing.connectionUrl", "");
		window.setMember("appletWrapperService", jsService);
		window.eval("webswing.$('head').append(webswing.$('<link rel=\"stylesheet\" type=\"text/css\" />').attr('href', '" + base_path + "tabs.css'));");
		window.eval("var appletjs = document.createElement(\"script\"); appletjs.src = '" + base_path + "applet.js'; document.body.appendChild(appletjs);");
	}

	public static void removeJavascript() {
		JSObject window = JSObject.getWindow(null);
		window.removeMember("appletWrapperService");
	}

	static String file2String(String name) {
		InputStream is = Main.class.getClassLoader().getResourceAsStream(name);
		if (is == null) {
			throw new IllegalStateException("Resource '" + name + "' not found.");
		}
		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}

	public static List<int[]> getWindowAreas() {
		ArrayList<int[]> result = new ArrayList<>();
		for (Window w : Window.getWindows()) {
			if (w.isShowing()) {
				Rectangle b = w.getBounds();
				int[] area = new int[] { b.x, b.y, b.x + b.width, b.y + b.height };
				result.add(area);
			}
		}
		return result;
	}

	public static boolean isModalFocused() {
		KeyboardFocusManager currentManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
		Window activeWindow = currentManager.getActiveWindow();
		if (activeWindow instanceof JDialog) {
			boolean modal = ((JDialog) activeWindow).isModal();
			return modal;
		}
		return false;
	}
}
