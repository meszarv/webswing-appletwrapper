package org.webswing.appletwrapper.model;

import java.util.List;

public interface SyncRequest {

	List<AppletSync> getSyncItems();
}
