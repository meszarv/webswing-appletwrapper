package org.webswing.appletwrapper.model;

public class AppletSyncError {

	private final String id;
	private final String error;

	public AppletSyncError(String id, String s) {
		this.id = id;
		this.error = s;
	}

	public String getId() {
		return id;
	}

	public String getError() {
		return error;
	}
}
