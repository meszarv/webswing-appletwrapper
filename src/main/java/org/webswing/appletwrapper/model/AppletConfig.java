package org.webswing.appletwrapper.model;

import org.webswing.appletwrapper.config.ConfigField;

import java.util.List;
import java.util.Map;

public interface AppletConfig {

	@ConfigField
	String getAppletClass();

	@ConfigField
	List<String> getClassPathEntries();

	@ConfigField
	Map<String, String> getParameters();
}
