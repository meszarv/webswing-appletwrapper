package org.webswing.appletwrapper.model;

public interface Position {

	int getTop();

	int getBottom();

	int getLeft();

	int getRight();
}
