package org.webswing.appletwrapper.model;

import java.util.ArrayList;
import java.util.List;

public class SyncResponse {

	List<int[]> windowAreas = new ArrayList<>();

	List<AppletSyncError> results = new ArrayList<>();
	private boolean modalFlag;

	public List<AppletSyncError> getResults() {
		return results;
	}

	public void setResults(List<AppletSyncError> results) {
		this.results = results;
	}

	public List<int[]> getWindowAreas() {
		return windowAreas;
	}

	public void setWindowAreas(List<int[]> windowAreas) {
		this.windowAreas = windowAreas;
	}

	public void setModalFlag(boolean modalFlag) {
		this.modalFlag = modalFlag;
	}

	public boolean isModalFlag() {
		return modalFlag;
	}
}
