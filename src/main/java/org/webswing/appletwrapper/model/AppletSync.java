package org.webswing.appletwrapper.model;

import java.util.Map;

public interface AppletSync {
	ActionType getAction();

	String getId();

	String getName();

	Map<String, String> getParameters();

	Position getPosition();

	boolean isVisible();
}
