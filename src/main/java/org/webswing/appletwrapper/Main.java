package org.webswing.appletwrapper;

import org.webswing.appletwrapper.config.ConfigService;
import org.webswing.appletwrapper.javascript.JsService;
import org.webswing.appletwrapper.javascript.JsUtil;
import org.webswing.toolkit.api.WebswingUtil;
import org.webswing.toolkit.api.lifecycle.WebswingShutdownListener;
import org.webswing.toolkit.api.security.UserEvent;
import org.webswing.toolkit.api.security.WebswingUserListener;
import org.webswing.toolkit.util.Logger;

import javax.swing.JOptionPane;

public class Main {
	public static JsService jsService;

	public static void main(String[] args) {
		if (WebswingUtil.isWebswing()) {
			if (args.length != 1) {
				Logger.info("Missing argument: Argument pointing to configuration file is expected.");
			}
			ConfigService configService = new ConfigService(args[0]);
			AppletManager appletManager = new AppletManager(configService);
			jsService = new JsService(appletManager);
			JsUtil.installJavascript(jsService);

			WebswingUtil.getWebswingApi().addUserConnectionListener(new WebswingUserListener() {
				@Override
				public void onPrimaryUserConnected(UserEvent userEvent) {
					JsUtil.installJavascript(jsService);
				}

				@Override
				public void onPrimaryUserDisconnected(UserEvent userEvent) {

				}

				@Override
				public void onMirrorViewConnected(UserEvent userEvent) {

				}

				@Override
				public void onMirrorViewDisconnected(UserEvent userEvent) {

				}
			});
			WebswingUtil.getWebswingApi().addShutdownListener(new WebswingShutdownListener() {
				@Override
				public void onShutdown() {
					appletManager.shutdown();
					if (WebswingUtil.getWebswingApi().getPrimaryUser() != null) {
						JsUtil.removeJavascript();
					}
					System.exit(0);
				}
			});

		} else {
			JOptionPane.showMessageDialog(null, "This application is designed to work inside Webswing container only.");
			Logger.error("This application is designed to work inside Webswing container only.");
			System.exit(1);
		}
	}

}
