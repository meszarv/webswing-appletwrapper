package org.webswing.appletwrapper;

import org.webswing.Constants;
import org.webswing.applet.AppletContainer;
import org.webswing.appletwrapper.model.AppletConfig;
import org.webswing.appletwrapper.model.Position;
import org.webswing.toolkit.util.ClasspathUtil;
import org.webswing.toolkit.util.Logger;
import org.webswing.toolkit.util.Util;

import javax.swing.RepaintManager;
import java.applet.Applet;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class AppletInstance {
	private final String id;
	private final AppletConfig config;
	private final Map<String, String> parameters = new HashMap<>();
	private AppletContainer ac;

	public AppletInstance(String id, AppletConfig config, Map<String, String> parameters) {
		this.id = id;
		this.config = config;
		if (config.getParameters() != null) {
			this.parameters.putAll(config.getParameters());
		}
		if (parameters != null) {
			this.parameters.putAll(parameters);
		}
	}

	public void updatePosition(Position p, boolean visible) {
		Logger.debug("Updating position of " + id);
		if (ac != null) {
			ac.setBounds(p.getLeft(), p.getTop(), p.getRight() - p.getLeft(), p.getBottom() - p.getTop());
			if (ac.isVisible() != visible) {
				ac.setVisible(visible);
			}
		}
	}

	public void start() throws Exception {
		try {
			Logger.info("Creating applet " + id + ". [" + config.getAppletClass() + " params:" + parameters + "]");
			String relativeBase = System.getProperty(Constants.SWING_START_SYS_PROP_APP_HOME);
			String cp = generateClassPathString(config.getClassPathEntries());
			URL[] urls = ClasspathUtil.populateClassPath(cp, relativeBase);
			ClassLoader appletClassLoader = new URLClassLoader(urls, getClass().getClassLoader());
			Class<?> appletClazz = appletClassLoader.loadClass(config.getAppletClass());
			if (Applet.class.isAssignableFrom(appletClazz)) {
				ac = new AppletContainer(appletClazz, parameters);
				ac.start();
			} else {
				throw new IllegalArgumentException("Failed to start applet: " + appletClazz.getCanonicalName() + " class is not a subclass of Applet");
			}
		} catch (Exception e) {
			ac = new AppletContainer(ErrorApplet.class, parameters);
			ac.start();
			Logger.error("Error", e);
		}
	}

	public static String generateClassPathString(Collection<String> classPathEntries) {
		String result = "";
		if (classPathEntries != null) {
			for (String cpe : classPathEntries) {
				result += cpe + ";";
			}
			result = result.length() > 0 ? result.substring(0, result.length() - 1) : result;
		}
		return result;
	}

	public void destroy() {
		Logger.info("Destroying applet " + id);
		if (ac != null) {
			try {
				ac.stop();
			} catch (Exception e) {
				Logger.warn("Failed to stop applet");
			}
			try {
				ac.getApplet().destroy();
			} catch (Exception e) {
				Logger.warn("Failed to destroy applet");
			}
			ac.setVisible(false);
			ac.dispose();
		}
	}

	public void setVisible(boolean visible) {
		ac.setVisible(visible);
	}
}
