package org.webswing.appletwrapper;

import org.webswing.appletwrapper.config.ConfigService;
import org.webswing.appletwrapper.model.AppletConfig;
import org.webswing.appletwrapper.model.AppletSyncError;
import org.webswing.appletwrapper.model.Position;
import org.webswing.toolkit.util.Logger;
import org.webswing.toolkit.util.Util;

import javax.swing.RepaintManager;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class AppletManager {

	private final ConfigService config;
	private final String initialUrl;
	private ConcurrentHashMap<String, AppletInstance> instances = new ConcurrentHashMap<>();

	public AppletManager(ConfigService config) {
		this.config = config;
		this.initialUrl = config.getInitialUrl();
	}

	public AppletSyncError createApplet(String name, String id, Position position, Map<String, String> parameters) {
		try {
			AppletConfig appletConfig = config.getAppletConfig(name);
			if (appletConfig != null) {
				AppletInstance instance = new AppletInstance(id, appletConfig, parameters);
				instance.start();
				instance.updatePosition(position, true);
				instances.put(id, instance);
			} else {
				throw new IllegalArgumentException("Applet configuration for '" + name + "' not found.");
			}
		} catch (Exception e) {
			Logger.error("Failed to create new applet instance", e);
			return new AppletSyncError(id, "Failed to create new applet instance: " + e.getMessage());
		}
		return null;
	}

	public AppletSyncError deleteApplet(String id) {
		try {
			AppletInstance instance = instances.get(id);
			if (instance != null) {
				instance.destroy();
				instances.remove(id);
			}
		} catch (Exception e) {
			Logger.error("Failed to delete applet instance", e);
			return new AppletSyncError(id, "Failed to delete applet instance: " + e.getMessage());
		}
		return null;
	}

	public AppletSyncError updateApplet(String id, Position position, boolean visible) {
		try {
			AppletInstance instance = instances.get(id);
			if (instance != null) {
				instance.updatePosition(position,visible);
			} else {
				throw new IllegalArgumentException("Applet with id '" + id + "' does not exist.");
			}
		} catch (Exception e) {
			Logger.error("Failed to update applet position", e);
			return new AppletSyncError(id, "Failed to update applet position: " + e.getMessage());
		}
		return null;
	}

	public String getInitialUrl() {
		return initialUrl;
	}

	public void shutdown() {
		Logger.info("AppletManager.shutdown()");
		if(instances != null) {
			Iterator itr = instances.keySet().iterator();
			while(itr.hasNext()) {
				AppletInstance instance = instances.get(itr.next());
				if(instance != null) {
					instance.destroy();
				}
			}
		}
	}
}
