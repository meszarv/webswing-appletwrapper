package org.webswing.appletwrapper.config;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(value = { ElementType.METHOD })
@Inherited
public @interface ConfigFieldDefaultValueBoolean {

	boolean value();
}
