package org.webswing.appletwrapper.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.webswing.appletwrapper.model.AppletConfig;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class ConfigService {
	private static final ObjectMapper mapper = new ObjectMapper();
	private final Map<String, Object> configMap;

	public ConfigService(String filename) {
		try {
			File configFile = new File(filename);
			if (configFile.isFile()) {
				configMap = mapper.readValue(configFile, Map.class);
			} else {
				throw new IllegalArgumentException("ConfigService file not found [" + configFile.getAbsolutePath() + "]");
			}
		} catch (IOException e) {
			throw new RuntimeException("Failed to initialize configuration", e);
		}
	}

	public Set<String> getApplets() {
		if (configMap != null) {
			return new LinkedHashSet<String>(configMap.keySet());
		}
		return new LinkedHashSet<String>();
	}

	public AppletConfig getAppletConfig(String applet) {
		if (configMap == null || configMap.get(applet) == null) {
			return null;
		} else {
			return ConfigUtil.instantiateConfig((Map<String, Object>) configMap.get(applet), AppletConfig.class);
		}
	}

	public String getInitialUrl() {
		if (configMap == null || configMap.get("initialUrl") == null) {
			throw new IllegalArgumentException("Failed to initialize configuration: initialUrl attribute is not defined.");
		} else {
			return (String) configMap.get("initialUrl");
		}
	}
}
