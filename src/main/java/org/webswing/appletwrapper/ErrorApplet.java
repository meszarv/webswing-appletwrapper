package org.webswing.appletwrapper;

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

public class ErrorApplet extends JApplet {
	@Override
	public void start() {
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		JPanel infopanel = new JPanel(new GridLayout(2, 1));
		infopanel.add(new JLabel("Initialization Error"));
//		JButton retry = new JButton("Retry");
//		retry.setAction(new AbstractAction() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//
//			}
//		});
//		infopanel.add(retry);
		infopanel.setBorder(BorderFactory.createTitledBorder("Error"));
		panel.add(BorderLayout.CENTER, infopanel);
		add(panel);
	}
}
