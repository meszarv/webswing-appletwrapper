(function () {
    var timer;
    var $ = webswing.$;
    var webswingAreas = [];
    var appletInstances = [];
    var tabs, tabContent;
    var modal;
    var mySessionId = '';

    var tabHistory = [];

    window.openNative = window.open;
    window.open = tabOpen;


    function install() {
        mySessionId = '';
        installEventHandlers();
        timer = setInterval(synchronize, 500);
        window.appletWrapperService.getInitialUrl().then(function (url) {
            if (location.search != null && location.search.length > 0) {
                var searchQuery = location.search.indexOf("?") == 0 ? location.search.substring(1) : locatin.search;
                if (url.indexOf('?') == -1) {
                    url = url + '?' + searchQuery;
                } else {
                    url = url + '&' + searchQuery;
                }
            }
            createTab(url, 'initial');
        }, function (error) {
            console.log(error);
        });
    }

    function createTab(url, name) {
        var creatingTab = true;
        var webswing = $('.webswing-element');
        if (tabs == null) {
            tabs = $('<ul class="tabs clearfix" data-tabgroup="tab-content">');
            tabs.insertAfter(webswing);
            tabContent = $('<div class="tabs-content"></div>');
            tabContent.insertBefore(webswing);
        }
        var li = $('<li></li>');
        var tabId = GUID();
        var a = $('<a data-id="' + tabId + '" data-name="' + name + '"><span class="tab-title">Loading...</span><span class="tab-close"> </span></a>');
        if (name == 'initial') {
            a.find('span.tab-close').remove();
        }

        //focus tab
        a.click(function (e) {
            e.preventDefault();
            if (a.hasClass('active')) {
                return;
            }
            if (!creatingTab && modal) {
                return;
            }
            //hide all
            tabs.find('a').removeClass('active');
            tabs.find('a span.tab-close').hide();
            tabs.find('span.tab-title').css('right', '0px');
            $('.tabFrame').hide();
            //show this
            a.addClass('active');
            a.find('span.tab-close').show();
            a.find('span.tab-title').css('right', '25px');
            iframe.show();
            window.innerWindow = iframe[0].contentWindow;

            removeItem(tabHistory, tabId);
            tabHistory.push(tabId);

            //clear canvas
            var canvas = $("[data-webswing-active] canvas")[0];
            canvas.width = canvas.width;
        });

        //close tab
        a.find('span.tab-close').click(function (e) {
            iframe.remove();
            a.parent().remove();
            removeItem(tabHistory, tabId);
            var lastTab = tabHistory[tabHistory.length - 1];
            if (lastTab != null) {
                $('a[data-id="' + lastTab + '"]').click();
            } else {
                tabs.find('a:first').click();
            }
        });

        var iframe = $('<iframe data-tabid="' + tabId + '" data-name="' + name + '" class="tabFrame"></iframe>').attr("src", url);
        iframe.on('load', function () {
            var title = iframe.contents().find("title").html();
            a.find('span.tab-title').text(title);
            a.attr("title", title + '\n' + url);
            iframe[0].contentWindow.open = tabOpen;
        });

        tabs.append(li.append(a));
        tabContent.append(iframe);
        a.click();
        creatingTab = false;
    }

    function tweakUrlWithMySessionId(url) {
        if(mySessionId.length > 0) { // if user is already signed on...
            url = url.split('#')[0]; // remove any trailing named anchor (we add our own below)
            if(url.indexOf('wssot=') == -1) { // if there is no wssot present...
                var prefixToMySessionIdArg = '&';
                if(url.indexOf('?') == -1) {
                    prefixToMySessionIdArg = '?';
                }
                url = url + prefixToMySessionIdArg + 'wssot=' + mySessionId;
            }
        }
        return url + '#wsTop';
    }

    function closeCurrentTab() {
        if (tabs != null) {
            tabs.find('a.active').find('span.tab-close').click();
        }
    }

    function setMySessionId(newMySessionId) {
        mySessionId = newMySessionId;
    }

    function closeAllAndReload(urlToLoad) {
        if (tabs != null) {
            var tmpTabsIds = tabHistory.slice();
            for (var i = 0; i < tmpTabsIds.length; i++) {
                var tabId = tmpTabsIds[i];
                $('a[data-id="' + tabId + '"]').click(); // focus tab (i.e., make it the current tab)
                closeCurrentTab(); // close focused tab
            }
        }
        if(urlToLoad != 'null') {
            window.open(urlToLoad, '_self');
        }
    }

    function synchronize() {
        var syncItems = []
        var currentInstances = [];
        var instancesToDelete = appletInstances.slice();
        var appletElements = findAppletsInIframes();
        for (var i in appletElements) {
            var name = appletElements[i].name;
            var element = $(appletElements[i].item);
            var appletId = element.data('wsAppletId');
            var position = getPosition(element);
            if (appletId == null) {
                //applet is new, create instance
                appletId = GUID();
                element.data('wsAppletId', appletId);
                syncItems.push(createAppletItem(name, appletId, position, getParameters(element)));
            } else {
                //update position of applet
                removeItem(instancesToDelete, appletId);
                syncItems.push(updateAppletItem(appletId, position, appletElements[i].visible));
            }
            currentInstances.push(appletId);
        }
        appletInstances = currentInstances;
        // removed applets with missing definition
        for (var i in instancesToDelete) {
            syncItems.push(deleteAppletItem(instancesToDelete[i]));
        }

        window.appletWrapperService.sync(JSON.stringify({syncItems: syncItems})).then(function (json) {
            if (json != null) {
                var response = JSON.parse(json);
                if (response != null && response.results != null) {
                    for (var i in response.results) {
                        var errstatus = response.results[i];
                        var element = $('[ws-applet-id="' + errstatus.id + '"]');
                        console.error('Sync failed: ' + errstatus.error, element.length > 0 ? element[0] : errstatus.id);
                    }
                }


                webswingAreas = response.windowAreas;
                var offset = getWebswingOffset();
                addWebswingDialogArea('div[data-id="copyBar"]', offset);
                addWebswingDialogArea('div[data-id="commonBar"]', offset);
                $("div.webswingWindow").off('.applet').remove();
                for (var i in webswingAreas) {
                    var a = webswingAreas[i];
                    $("div.tabs-content").append($('<div class="webswingWindow"/>')
                        .css("top", (a[1] + offset.top) + "px")
                        .css("left", (a[0] + offset.left) + "px")
                        .css("width", (a[2] - a[0]) + "px")
                        .css("height", (a[3] - a[1]) + "px")
                    )
                }
                modal = response.modalFlag;
                installEventHandlers();
            }
        }, function (error) {
            console.log(error);
        });
    }

    function addWebswingDialogArea(selector, webswingOffset) {
        var e = $(selector + ":visible");
        if (e.length > 0) {
            var left = e.offset().left - webswingOffset.left;
            var top = e.offset().top - webswingOffset.top;
            webswingAreas.push([left, top, left + e.outerWidth(), top + e.outerHeight()]);
        }
    }

    function installEventHandlers() {
        var layers = $.merge($("[data-webswing-active] canvas"), $("div.webswingWindow"));
        layers.off('.applet');
        layers.on('mousemove.applet', function (e) {
            var offset = getWebswingOffset();
            var x = e.pageX - offset.left, y = e.pageY - offset.top;
            for (var i in webswingAreas) {
                var a = webswingAreas[i];
                if (x > a[0] && x < a[2] && y > a[1] && y < a[3]) {
                    focusWebswing();
                    return;
                }
            }
            if (!modal) {
                focusHtml();
            }
        });
    }

    function focusWebswing() {
        $('[data-webswing-active]').addClass('focusedLayer');
        $("div.tabs-content").removeClass('focusedLayer');
    }

    function focusHtml() {
        $('[data-webswing-active]').removeClass('focusedLayer');
        $("div.tabs-content").addClass('focusedLayer');
    }

    function tabOpen(url, target, specs, replace, nativeTarget) {
        url = tweakUrlWithMySessionId(url);
        if (target == null || target === '_blank') {
            createTab(url);
        } else if (target === '_native') {
            window.openNative(url, nativeTarget, specs, replace);
        } else if (target === '_self') {
            $('iframe.tabFrame:visible').attr('src', url);
        } else {
            var namedFrame = $('iframe.tabFrame[data-name="' + target + '"]');
            if (namedFrame.length > 0) {
                namedFrame.attr('src', url);
                $('a[data-name="' + target + '"]').click();
            } else {
                createTab(url, target);
            }
        }
    }

    function createAppletItem(name, id, position, parameters) {
        return {
            action: 'create',
            id: id,
            name: name,
            position: position,
            parameters: parameters
        }
    }

    function updateAppletItem(id, position, visible) {
        return {
            action: 'update',
            id: id,
            position: position,
            visible: visible
        }
    }

    function deleteAppletItem(id) {
        return {
            action: 'delete',
            id: id
        }
    }

    function getPosition(el) {
        var item = $(el);
        var offset = getWebswingOffset();
        var pos = item[0].getBoundingClientRect();
        var ifpos = item[0].ownerDocument.defaultView.wsIframePosition;
        return {
            top: pos.top + ifpos.top - offset.top,
            left: pos.left + ifpos.left - offset.left,
            bottom: pos.bottom + ifpos.top - offset.top,
            right: pos.right + ifpos.left - offset.left
        }
    }

    function getWebswingOffset() {
        var offsetTop = parseFloat(getComputedStyle(tabContent[0], null).getPropertyValue('padding-top'));
        var offsetLeft = parseFloat(getComputedStyle(tabContent[0], null).getPropertyValue('padding-left'));
        return {
            top: offsetTop,
            left: offsetLeft
        }
    }

    function getParameters(el) {
        var result = {};
        var item = $(el);
        var parameters = item.find('webswing-param');
        parameters.each(function (i, parameter) {
            var name = $(parameter).attr('name');
            var value = $(parameter).attr('value');
            if (name != null) {
                result[name] = value;
            }
        });
        return result;

    }

    function findIframes(parent, visible) {
        var ifpos = parent != null ? parent.getBoundingClientRect() : {top: 0, left: 0, bottom: 0, right: 0};
        var visibility = visible == null ? '' : (visible ? ':visible' : ':hidden');
        var iframes = $('iframe' + visibility, parent != null ? $(parent).contents().find('body') : null);
        iframes.each(function (i, iframe) {
            var pos = iframe.getBoundingClientRect();
            iframe.contentWindow.wsIframePosition = {
                top: pos.top + ifpos.top,
                left: pos.left + ifpos.left,
                bottom: pos.bottom + ifpos.top,
                right: pos.right + ifpos.left
            }
            if (visible) {
                $.merge(iframes, findIframes(iframe, visible));
            } else {
                $.merge(iframes, findIframes(iframe));
            }
        });
        return iframes;
    }

    function findAppletsInIframes() {
        result = [];

        function scanIframes(visible) {
            var iframes = findIframes(null, visible);
            iframes.each(function (i, iframe) {
                var items = $('[data-ws-applet-name]', $(iframe).contents());
                items.each(function (j, item) {
                    var name = $(item).data("wsAppletName");
                    result.push({
                        name: name,
                        item: item,
                        visible: visible
                    });
                });
            });
        }

        scanIframes(true);
        scanIframes(false);
        return result;
    }

    function GUID() {
        var S4 = function () {
            return Math.floor(Math.random() * 0x10000).toString(16);
        };
        return (S4() + S4() + S4());
    }

    function removeItem(array, item) {
        var index = array.indexOf(item);
        if (index > -1) {
            return array.splice(index, 1)[0];
        }
    }

    if (window.webswingAppletWrapper == null) {
        install();
        window.webswingAppletWrapper = {
            createTab: createTab,
            focusWebswing: focusWebswing,
            focusHtml: focusHtml,
            closeCurrentTab: closeCurrentTab,
            setMySessionId: setMySessionId,
            tweakUrlWithMySessionId: tweakUrlWithMySessionId,
            closeAllAndReload: closeAllAndReload
            // To call from applet, use: window.webswingAppletWrapper.setMySessionId('someId');
        };
    }

})();